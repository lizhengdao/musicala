![Logo](https://gitlab.com/matthias-weiss/musicala/raw/HEAD/app/src/main/res/mipmap-xxxhdpi/ic_launcher.png)
# musicala - Android MPD Client #

[<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/app/at.weiss.matthias.musicala)

This whole project is licensed under the  **GPLv3 or later** license (see LICENSE)

## Requirements: ##
 - Android 5.0
 - MPD >= 0.14
 
## Features: ##
 - Artist/Album/Tracks library browsing
 - Launcher widget
 - Notification with background service (**optional, can be disabled in the settings**)
 - Volume control if application is in background (**only if notification is enabled & visible**)
 - For tablets only
 - Basic playlist management (add songs to the list, replace list with new one)
 - Multiple server profiles
 - When cover art is available in your music library musicala can access it via HTTP

## Description ##

This is musicala, a fork of the MPD client M.A.L.P. (https://gitlab.com/gateship-one/malp), with emphasis on easy use. This app is intended to be used by on tablets only.

This MPD client is tested with MPD 0.14, 0.19 and 0.20 but you really should not use outdated versions of MPD.

**This MPD client works best with accurately taged music libraries. I recommend tagging using [MusicBrainz Picard](https://picard.musicbrainz.org/)** 

Starting with MPD 0.19 it is possible to filter list requests which allows this client to use the AlbumArtist and Artist tag for retrieval of albums. So if you have a lot of "Greatest Hits" albums you will get a list entry for each one of it. 
