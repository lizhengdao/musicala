/*
 *  Copyright (C) 2018 Team Gateship-One
 *  (Hendrik Borghorst & Frederik Luetkes)
 *
 *  The AUTHORS.md file contains a detailed contributors list:
 *  <https://gitlab.com/matthias-weiss/musicala/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package at.weiss.matthias.musicala.application.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;

import at.weiss.matthias.musicala.R;
import at.weiss.matthias.musicala.application.utils.App;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        App.setContext(this);

        // check whether devices screen is big enough
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int minWidthDp  = (int)((metrics.widthPixels < metrics.heightPixels ? metrics.widthPixels : metrics.heightPixels) / (metrics.densityDpi / 160));

        Log.d(TAG, "minimal width in dp: " + minWidthDp);

        if (minWidthDp < 600) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getResources().getString(R.string.screen_too_small_dialog_title));
            builder.setMessage(getResources().getString(R.string.screen_too_small_dialog_text));

            builder.setPositiveButton(R.string.dialog_action_ok, (dialog, id) -> {
                dialog.dismiss();
                System.exit(0);
            });

            builder.setCancelable(false);

            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Bundle extras = getIntent().getExtras();

            Intent intent = new Intent(this, MainActivity.class);
            if (extras != null) {
                intent.putExtras(extras);
            }
            startActivity(intent);
            finish();
        }
    }
}
