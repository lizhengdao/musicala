/*
 *  Copyright (C) 2018 Matthias Weiss
 *
  *  <https://gitlab.com/matthias-weiss/musicala/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package at.weiss.matthias.musicala.application.adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import at.weiss.matthias.musicala.R;
import at.weiss.matthias.musicala.application.artworkdatabase.ArtworkManager;
import at.weiss.matthias.musicala.application.listviewitems.LibraryItem;
import at.weiss.matthias.musicala.application.listviewitems.LibraryViewHolder;
import at.weiss.matthias.musicala.mpdservice.handlers.responsehandler.MPDResponseAlbumList;
import at.weiss.matthias.musicala.mpdservice.handlers.responsehandler.MPDResponseArtistList;
import at.weiss.matthias.musicala.mpdservice.handlers.responsehandler.MPDResponseFileList;
import at.weiss.matthias.musicala.mpdservice.handlers.serverhandler.MPDQueryHandler;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDAlbum;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDArtist;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDFileEntry;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDTrack;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class LibraryAdapter extends RecyclerView.Adapter<LibraryViewHolder> {

    private static final String TAG = LibraryAdapter.class.getSimpleName();
    private boolean mUseAlbumArtists;
    private boolean mUseArtistSort;

    private MPDResponseArtistList pArtistResponseHandler;
    private MPDResponseAlbumList  pAlbumResponseHandler;
    private MPDResponseFileList   pTrackResponseHandler;
    private ArtworkManager        mArtworkManager;

    private final ArrayList<LibraryItem>    mList = new ArrayList<>();
    private final Context                   mContext;
    private int                             mLevelIndicatorWidth = 0;
    private final Vector<ExpandedItem>      mExpanded = new Vector<>();
    private RecyclerView                    mRecyclerView;


    public class ExpandedItem {
        public LibraryItem        mItem;
        public int                mPosition;
        public int                mNrOfChildren;

        public ExpandedItem(LibraryItem item, int position, int nrOfChildren) {
            mItem         = item;
            mPosition     = position;
            mNrOfChildren = nrOfChildren;
        }
    }

    private static class ArtistResponseHandler extends MPDResponseArtistList {
        private WeakReference<LibraryAdapter> mAdapter;

        private ArtistResponseHandler(LibraryAdapter adapter) {
            mAdapter = new WeakReference<>(adapter);
        }

        @Override
        public void handleArtists(List<MPDArtist> artistList) {
            LibraryAdapter adapter = mAdapter.get();
            if (adapter != null) {
                adapter.updateArtists(artistList);
                adapter.notifyDataSetChanged();
            }
        }
    }

    public static class AlbumResponseHandler extends MPDResponseAlbumList {
        private WeakReference<LibraryAdapter> mAdapter;

        private AlbumResponseHandler(LibraryAdapter adapter) {
            mAdapter = new WeakReference<>(adapter);
        }

        @Override
        public void handleAlbums(List<MPDAlbum> albumList, int position) {
            LibraryAdapter adapter = mAdapter.get();

            if (adapter != null) {
                adapter.insertChildren(albumList, position);
            }
        }
    }

    public static class TrackResponseHandler extends MPDResponseFileList {
        private WeakReference<LibraryAdapter> mAdapter;

        private TrackResponseHandler(LibraryAdapter adapter) {
            mAdapter = new WeakReference<>(adapter);
        }

        @Override
        public void handleTracks(List<MPDFileEntry> fileList, int start, int end, int position) {
            LibraryAdapter adapter = mAdapter.get();
            List<MPDTrack> tracklist = new ArrayList<>();

            for(MPDFileEntry file: fileList) {
                if (file instanceof MPDTrack) {
                    tracklist.add((MPDTrack)file);
                }
            }

            if (adapter != null && tracklist.size() > 0) {
                adapter.insertChildren(tracklist, position);
            }
        }
    }

    public LibraryAdapter(Context context, RecyclerView recyclerView, boolean useAlbumArtists, boolean useArtistSort) {
        super();

        mContext             = context;
        mRecyclerView        = recyclerView;
        mLevelIndicatorWidth = 96;
        mUseAlbumArtists     = useAlbumArtists;
        mUseArtistSort       = useArtistSort;

        pArtistResponseHandler = new LibraryAdapter.ArtistResponseHandler(this);
        pAlbumResponseHandler  = new LibraryAdapter.AlbumResponseHandler(this);
        pTrackResponseHandler  = new LibraryAdapter.TrackResponseHandler(this);

        initializeList();

        mArtworkManager = ArtworkManager.getInstance(mContext.getApplicationContext());
    }

    public void initializeList() {
        mExpanded.clear();
        mList.clear();

        if( !mUseAlbumArtists) {
            if(!mUseArtistSort) {
                MPDQueryHandler.getArtists(pArtistResponseHandler);
            } else {
                MPDQueryHandler.getArtistSort(pArtistResponseHandler);
            }
        } else {
            if(!mUseArtistSort) {
                MPDQueryHandler.getAlbumArtists(pArtistResponseHandler);
            } else {
                MPDQueryHandler.getAlbumArtistSort(pArtistResponseHandler);
            }
        }
    }

    @Override
    public LibraryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //Log.v(TAG, "onCreateViewHolder");
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item_library, parent, false);

        LibraryViewHolder vh = new LibraryViewHolder(v, viewType, mContext, mArtworkManager);

        final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)
                vh.itemView.getLayoutParams();
        params.leftMargin = mLevelIndicatorWidth * viewType;
        vh.itemView.setLayoutParams(params);

        return vh;
    }

    @Override
    public void onBindViewHolder(final LibraryViewHolder holder, final int position) {

        //Log.v(TAG, "onBindViewHolder");
        final LibraryItem item = mList.get(position);

        item.setViewHolder(holder);

        final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)
                holder.itemView.getLayoutParams();
        params.leftMargin = mLevelIndicatorWidth * item.getLevel();
        holder.itemView.setLayoutParams(params);

        if (item.getViewType() != MPDArtist.VIEW_TYPE && item.isExpanded()) {
            showAdd2PlaylistButtons(holder);
        } else {
            hideAdd2PlaylistButtons(holder);
        }

        holder.mMainText.setText(item.getMainText());

        //Log.v(TAG, "Item name: " + item.getMainText());
        if (mRecyclerView.getScrollState() == RecyclerView.SCROLL_STATE_IDLE) {
            if (item.getViewType() != MPDTrack.VIEW_TYPE) {
                //Log.v(TAG, "onBindViewHolder: State - IDLE: loadImage");
                holder.mImage.setImageResource(R.drawable.cover_placeholder_128dp);
                holder.loadImage(item);
            }
        } else {
            //Log.v(TAG, "onBindViewHolder: State - Scrolling: set Image");
            holder.mImage.setImageResource(R.drawable.cover_placeholder_128dp);
        }

        switch (item.getViewType()) {
            case MPDArtist.VIEW_TYPE:
                if (item.isExpanded()) {
                    holder.setItemColors(MPDArtist.VIEW_TYPE, true);
                } else {
                    holder.setItemColors(MPDArtist.VIEW_TYPE, false);
                }
                holder.mImage.setVisibility(View.VISIBLE);
                holder.mPrefixText.setVisibility(View.GONE);
                holder.mPostfixText.setVisibility(View.GONE);
                break;
            case MPDAlbum.VIEW_TYPE:
                holder.setItemColors(MPDAlbum.VIEW_TYPE, false);
                holder.mImage.setVisibility(View.VISIBLE);
                holder.mPrefixText.setVisibility(View.GONE);
                holder.mPostfixText.setVisibility(View.VISIBLE);
                holder.mPostfixText.setText(item.getPostfixText());

                holder.mPlayReplace.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MPDAlbum album = (MPDAlbum)item;
                        MPDQueryHandler.playArtistAlbum(album.getName(), album.getArtistName(), album.getMBID());
                    }
                });
                holder.mPlayInsertAfterCursor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MPDTrack track;

                        for (int i = (position + mExpanded.get(item.getLevel()).mNrOfChildren); i > position; i--) {
                            track = (MPDTrack)mList.get(i);
                            MPDQueryHandler.playSongNext(track.getPath());
                        }
                    }
                });
                holder.mPlayAppend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MPDAlbum album = (MPDAlbum)item;
                        MPDQueryHandler.addArtistAlbum(album.getName(), album.getArtistName(), album.getMBID());
                    }
                });

                break;
            case MPDTrack.VIEW_TYPE:
                holder.setItemColors(MPDTrack.VIEW_TYPE, false);
                holder.mImage.setVisibility(View.GONE);
                holder.mPrefixText.setVisibility(View.VISIBLE);
                holder.mPrefixText.setText(item.getPrefixText() + " -");
                holder.mPostfixText.setVisibility(View.VISIBLE);
                holder.mPostfixText.setText(item.getPostfixText());

                holder.mPlayReplace.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MPDTrack track = (MPDTrack)item;
                        MPDQueryHandler.clearPlaylist();
                        MPDQueryHandler.playSong(track.getPath());
                    }
                });
                holder.mPlayInsertAfterCursor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MPDTrack track = (MPDTrack)item;
                        MPDQueryHandler.playSongNext(track.getPath());
                    }
                });
                holder.mPlayAppend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MPDTrack track = (MPDTrack)item;
                        MPDQueryHandler.addPath(track.getPath());
                    }
                });

                break;
        }

        holder.mItemContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (item.isExpanded()) {
                            collapseItem(position);
                        } else {
                            expandItem(item, holder);
                        }
                    }
                });
            }
        });

    }

    private void hideAdd2PlaylistButtons(LibraryViewHolder holder) {
        holder.mPlayReplace.setVisibility(View.GONE);
        holder.mPlayInsertAfterCursor.setVisibility(View.GONE);
        holder.mPlayAppend.setVisibility(View.GONE);
    }

    private void showAdd2PlaylistButtons(LibraryViewHolder holder) {
        holder.mPlayReplace.setVisibility(View.VISIBLE);
        holder.mPlayInsertAfterCursor.setVisibility(View.VISIBLE);
        holder.mPlayAppend.setVisibility(View.VISIBLE);
    }

    private void expandItem(LibraryItem item, LibraryViewHolder holder) {

        if(mRecyclerView == null) {
            return; // adapter detached
        }

        // before expanding an element, ensure there is no other element expanded at the same
        // level; the index of elements in mExpanded also represents their level
        if (mExpanded.size() > item.getLevel()) {
            ExpandedItem exItem = mExpanded.get(item.getLevel());
            collapseItem(exItem.mPosition);
        }

        int position = mList.indexOf(item);

        mExpanded.add(item.getLevel(), new ExpandedItem(item, position, 0));

        switch (item.getViewType()) {
            case MPDArtist.VIEW_TYPE:
                item.getKidItems(pAlbumResponseHandler, position);
                notifyItemChanged(position);
                break;
            case MPDAlbum.VIEW_TYPE:
                item.getKidItems(pTrackResponseHandler, position);
                break;
            case MPDTrack.VIEW_TYPE:
                item.setExpanded(true);
                showAdd2PlaylistButtons(holder);
                notifyItemChanged(position);
                break;
        }
    }

    private void collapseItem(int position) {
        LibraryItem item = mList.get(position);
        if (!item.isExpanded()) {
            return;
        }

        if (item.getViewType() == MPDArtist.VIEW_TYPE) {
            item.getViewHolder().setItemColors(MPDArtist.VIEW_TYPE, false);
            notifyItemChanged(position);
        }
        int remove_count = 0;
        ExpandedItem exItem;

        while (mExpanded.size() > item.getLevel()) {
            exItem = mExpanded.remove(mExpanded.size() - 1);
            if (exItem.mNrOfChildren > 0) {
                remove_count += exItem.mNrOfChildren;
                mList.subList(exItem.mPosition + 1, exItem.mPosition + 1 + exItem.mNrOfChildren).clear();
            }
            exItem.mItem.setExpanded(false);

            if (exItem.mItem.getViewType() != MPDArtist.VIEW_TYPE) {
                hideAdd2PlaylistButtons(exItem.mItem.getViewHolder());
                notifyItemChanged(exItem.mPosition);
            }

        }

        item.setExpanded(false);

        if (remove_count > 0) {
            notifyItemRangeRemoved(position + 1, remove_count);
        }
    }

    public void updateArtists(List<MPDArtist> artistList) {
        if (mExpanded.size() > 0) {
            mExpanded.clear();
        }
        mList.clear();
        mList.addAll(artistList);
    }

    public void insertChildren(List<? extends LibraryItem> childList, int position) {
        LibraryItem item = mList.get(position);
        int nrOfChildren = childList.size();

        mExpanded.get(item.getLevel()).mNrOfChildren = nrOfChildren;

        if (nrOfChildren > 0) {
            mList.addAll(position + 1, childList);
            notifyItemRangeInserted(position + 1, nrOfChildren);
        }

        item.setExpanded(true);

        switch (item.getViewType()) {
            case MPDAlbum.VIEW_TYPE:
                showAdd2PlaylistButtons(item.getViewHolder());
                notifyItemChanged(position);
                break;
            case MPDArtist.VIEW_TYPE:
                item.getViewHolder().setItemColors(MPDArtist.VIEW_TYPE, true);
                notifyItemChanged(position);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void loadImages(int positionStart, int positionEnd) {

        LibraryViewHolder viewHolder;
        LibraryItem item;

        for (int i = positionStart; i <= positionEnd; i++) {
            item =  mList.get(i);
            if (item.getViewType() != MPDTrack.VIEW_TYPE) {
                viewHolder = mList.get(i).getViewHolder();
                if (viewHolder != null) {
                    viewHolder.loadImage(item);
                }
            }
        }
    }
}
