package at.weiss.matthias.musicala.application.fragments.serverfragments;

public interface ConnectionEstablishedRefreshListener {
    void refresh();
}
