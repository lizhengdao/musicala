package at.weiss.matthias.musicala.application.fragments.serverfragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.lang.ref.WeakReference;

import at.weiss.matthias.musicala.R;
import at.weiss.matthias.musicala.application.adapters.LibraryAdapter;
import at.weiss.matthias.musicala.mpdservice.handlers.MPDConnectionStateChangeHandler;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.MPDInterface;

public class LibraryFragment extends Fragment implements ConnectionEstablishedRefreshListener {

    private static final String TAG = LibraryAdapter.class.getSimpleName();
    protected ConnectionStateListener mConnectionStateListener;

    private LinearLayoutManager mLayoutManager;
    private LibraryAdapter mLibraryAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());

        FrameLayout rootView = (FrameLayout) inflater.inflate(R.layout.audio_source_fragment, container, false);
        RecyclerView recyclerView = rootView.findViewById(R.id.audio_source_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        boolean useAlbumArtists = sharedPref.getBoolean(getString(R.string.pref_use_album_artists_key), getResources().getBoolean(R.bool.pref_use_album_artists_default));
        boolean useArtistSort   = sharedPref.getBoolean(getString(R.string.pref_use_artist_sort_key), getResources().getBoolean(R.bool.pref_use_artist_sort_default));
        mLibraryAdapter         = new LibraryAdapter(getContext(), recyclerView, useAlbumArtists, useArtistSort);
        recyclerView.setAdapter(mLibraryAdapter);
        LibraryOnScrollListener onScrollListener = new LibraryOnScrollListener();
        recyclerView.addOnScrollListener(onScrollListener);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //Log.v(TAG, "onResume");
        refresh();
        Activity activity = getActivity();
        if (activity != null) {
            mConnectionStateListener = new LibraryFragment.ConnectionStateListener(this, activity.getMainLooper());
            MPDInterface.mInstance.addMPDConnectionStateChangeListener(mConnectionStateListener);
        }
        //ArtworkManager.getInstance(getContext().getApplicationContext()).registerOnNewArtistImageListener(mArtistAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
        //Log.v(TAG, "onPause");
        synchronized (this) {
            MPDInterface.mInstance.removeMPDConnectionStateChangeListener(mConnectionStateListener);
            mConnectionStateListener = null;
        }
        //ArtworkManager.getInstance(getContext().getApplicationContext()).unregisterOnNewArtistImageListener(mArtistAdapter);
    }

    @Override
    public void refresh() {
        if (! isDetached()) {
            mLibraryAdapter.initializeList();
        }
    }

    private static class ConnectionStateListener extends MPDConnectionStateChangeHandler {
        private WeakReference<LibraryFragment> pFragment;

        public ConnectionStateListener(LibraryFragment fragment, Looper looper) {
            super(looper);
            pFragment = new WeakReference<>(fragment);
        }

        @Override
        public void onConnected() {
            pFragment.get().refresh();
        }

        @Override
        public void onDisconnected() {
            LibraryFragment fragment = pFragment.get();
            if(fragment == null) {
                return;
            }
            synchronized (fragment) {
                if (!fragment.isDetached()) {
                    if(fragment.getLoaderManager().hasRunningLoaders()) {
                        fragment.getLoaderManager().destroyLoader(0);
                    }
                }
            }
        }
    }

    private class LibraryOnScrollListener extends RecyclerView.OnScrollListener {

        private final String TAG = LibraryOnScrollListener.class.getSimpleName();

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                Log.v(TAG, "onScrollStateChanged: loadImages");
                mLibraryAdapter.loadImages(mLayoutManager.findFirstVisibleItemPosition(),
                                           mLayoutManager.findLastVisibleItemPosition());
            }
        }
    }
}
