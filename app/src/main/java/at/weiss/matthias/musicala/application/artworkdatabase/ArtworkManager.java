/*
 *  Copyright (C) 2018 Team Gateship-One
 *  (Hendrik Borghorst & Frederik Luetkes)
 *
 *  The AUTHORS.md file contains a detailed contributors list:
 *  <https://gitlab.com/matthias-weiss/musicala/blob/master/AUTHORS.md>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package at.weiss.matthias.musicala.application.artworkdatabase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

import at.weiss.matthias.musicala.R;
import at.weiss.matthias.musicala.application.artworkdatabase.network.MALPRequestQueue;
import at.weiss.matthias.musicala.application.artworkdatabase.network.artprovider.HTTPImageProvider;
import at.weiss.matthias.musicala.application.artworkdatabase.network.responses.AlbumFetchError;
import at.weiss.matthias.musicala.application.artworkdatabase.network.responses.AlbumImageResponse;
import at.weiss.matthias.musicala.application.artworkdatabase.network.responses.ArtistFetchError;
import at.weiss.matthias.musicala.application.artworkdatabase.network.responses.ArtistImageResponse;
import at.weiss.matthias.musicala.application.utils.BitmapUtils;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDAlbum;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDArtist;
import at.weiss.matthias.musicala.mpdservice.mpdprotocol.mpdobjects.MPDTrack;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class ArtworkManager implements ArtistFetchError, AlbumFetchError {
    private static final String TAG = ArtworkManager.class.getSimpleName();

    /**
     * Maximmum size for either x or y of an image
     */
    private static final int MAXIMUM_IMAGE_RESOLUTION = 500;

    /**
     * Compression level if images are rescaled
     */
    private static final int IMAGE_COMPRESSION_SETTING = 80;

    /**
     * Maximum size of an image blob to insert in SQLite database. (1MB)
     */
    private static final int MAXIMUM_IMAGE_SIZE = 1024*1024;

    /**
     * Manager for the SQLite database handling
     */
    private ArtworkDatabaseManager mDBManager;

    /**
     * List of observers that needs updating if a new ArtistImage is downloaded.
     */
    private final ArrayList<onNewArtistImageListener> mArtistListeners;

    /**
     * List of observers that needs updating if a new AlbumImage is downloaded.
     */
    private final ArrayList<onNewAlbumImageListener> mAlbumListeners;

    /**
     * Private static singleton instance that can be used by other classes via the
     * getInstance method.
     */
    private static ArtworkManager mInstance;

    /**
     * Private {@link Context} used for all kinds of things like Broadcasts.
     * It is using the ApplicationContext so it should be safe against
     * memory leaks.
     */
    private Context mContext;

    /**
     * Settings value if artwork download is only allowed via wifi/wired connection.
     */
    private boolean mWifiOnly;

    /*
     * Broadcast constants
     */
    public static final String ACTION_NEW_ARTWORK_READY = "at.weiss.matthias.musicala.action_new_artwork_ready";

    public static final String INTENT_EXTRA_KEY_ARTIST_NAME = "at.weiss.matthias.musicala.extra.artist_name";

    public static final String INTENT_EXTRA_KEY_ALBUM_NAME = "at.weiss.matthias.musicala.extra.album_name";

    private ArtworkManager(Context context) {

        mDBManager = ArtworkDatabaseManager.getInstance(context.getApplicationContext());

        mArtistListeners = new ArrayList<>();
        mAlbumListeners = new ArrayList<>();


        mContext = context.getApplicationContext();

        ConnectionStateReceiver receiver = new ConnectionStateReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        mContext.registerReceiver(receiver, filter);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        mWifiOnly = sharedPref.getBoolean(context.getString(R.string.pref_download_wifi_only_key), context.getResources().getBoolean(R.bool.pref_download_wifi_default));
    }

    public static synchronized ArtworkManager getInstance(Context context) {
        if (null == mInstance) {
            mInstance = new ArtworkManager(context);
        }
        return mInstance;
    }

    public void setWifiOnly(boolean wifiOnly) {
        mWifiOnly = wifiOnly;
    }

    public void initialize(boolean wifiOnly) {
        mWifiOnly = wifiOnly;
    }


    /**
     * Removes the image for the album and tries to reload it from the internet
     *
     * @param album {@link MPDAlbum} to reload the image for
     */
    public void resetAlbumImage(final MPDAlbum album) {
        if (null == album) {
            return;
        }

        // Clear the old image
        mDBManager.removeAlbumImage(mContext, album);

        // Reload the image from the internet
        fetchAlbumImage(album);
    }


    /**
     * Removes the image for the artist and tries to reload it from the internet
     *
     * @param artist {@link MPDArtist} to reload the image for
     */
    public void resetArtistImage(final MPDArtist artist) {
        if (null == artist) {
            return;
        }

        // Clear the old image
        mDBManager.removeArtistImage(mContext, artist);

        // Reload the image from the internet
        fetchArtistImage(artist);
    }

    /**
     * Returns an artist image for the given artist.
     *
     * @param artist {@link MPDArtist} to get the image for-
     * @return The image if found or null if it is not available and has been tried to download before.
     * @throws ImageNotFoundException If the image is not found and was not searched before.
     */
    public Bitmap getArtistImage(final MPDArtist artist, int width, int height, boolean skipCache) throws ImageNotFoundException {
        if (null == artist) {
            return null;
        }

        if(!skipCache) {
            // Try cache first
            Bitmap cacheBitmap = BitmapCache.getInstance().requestArtistImage(artist);
            if (cacheBitmap != null && width <= cacheBitmap.getWidth() && height <= cacheBitmap.getWidth()) {
                return cacheBitmap;
            }
        }

        String image = mDBManager.getArtistImage(mContext, artist);

        // Checks if the database has an image for the requested artist
        if (null != image) {
            // Create a bitmap from the data blob in the database
            Bitmap bm = BitmapUtils.decodeSampledBitmapFromFile(image, width, height);
            BitmapCache.getInstance().putArtistImage(artist, bm);
            return bm;
        }
        return null;
    }

    /**
     * Returns an album image for the given album name and artist name.
     *
     * @param albumName  Name of the album to look for
     * @param artistName Name of the albums artists
     * @return The image if found or null if it is not available and has been tried to download before.
     * @throws ImageNotFoundException If the image is not found and was not searched before.
     */
    public Bitmap getAlbumImageFromAlbumNameArtistName(final String albumName, final String artistName, int width, int height, boolean skipCache) throws ImageNotFoundException {
        if (null == albumName || null == artistName) {
            return null;
        }

        if(!skipCache) {
            // Try cache first
            Bitmap cacheBitmap = BitmapCache.getInstance().requestAlbumBitmap(albumName, artistName);
            if (cacheBitmap != null && width <= cacheBitmap.getWidth() && height <= cacheBitmap.getWidth()) {
                return cacheBitmap;
            }
        }

        String image = mDBManager.getAlbumImage(mContext, albumName, artistName);

        // Checks if the database has an image for the requested album
        if (null != image) {
            // Create a bitmap from the data blob in the database
            Bitmap bm = BitmapUtils.decodeSampledBitmapFromFile(image, width, height);
            BitmapCache.getInstance().putAlbumBitmap(albumName, artistName, bm);
            return bm;
        }
        return null;
    }

    /**
     * Returns an album image for the given album name.
     *
     * @param albumName Name of the album to look for
     * @return The image if found or null if it is not available and has been tried to download before.
     * @throws ImageNotFoundException If the image is not found and was not searched before.
     */
    public Bitmap getAlbumImageFromName(final String albumName, int width, int height) throws ImageNotFoundException {
        if (null == albumName) {
            return null;
        }

        String image = mDBManager.getAlbumImage(mContext, albumName);

        // Checks if the database has an image for the requested album
        if (null != image) {
            // Create a bitmap from the data blob in the database
            return BitmapUtils.decodeSampledBitmapFromFile(image, width, height);
        }
        return null;
    }

    /**
     * Returns an album image for the given {@link MPDAlbum}
     *
     * @param album {@link MPDAlbum} to get the image for.
     * @return The image if found or null if it is not available and has been tried to download before.
     * @throws ImageNotFoundException If the image is not found and was not searched before.
     */
    public Bitmap getAlbumImage(final MPDAlbum album, int width, int height, boolean skipCache) throws ImageNotFoundException {
        if (null == album) {
            return null;
        }

        if(!skipCache) {
            // Try cache first
            Bitmap cacheBitmap = BitmapCache.getInstance().requestAlbumBitmap(album);
            if (null != cacheBitmap && width <= cacheBitmap.getWidth() && height <= cacheBitmap.getWidth()) {
                return cacheBitmap;
            }
        }

        String image = mDBManager.getAlbumImage(mContext, album.getName());

        // Checks if the database has an image for the requested album
        if (null != image) {
            // Create a bitmap from the data blob in the database
            Bitmap bm = BitmapUtils.decodeSampledBitmapFromFile(image, width, height);
            BitmapCache.getInstance().putAlbumBitmap(album, bm);
            return bm;
        }
        return null;
    }

    /**
     * Returns an album image for the given track.
     *
     * @param track {@link MPDTrack} to get the album image for.
     * @return The image if found or null if it is not available and has been tried to download before.
     * @throws ImageNotFoundException If the image is not found and was not searched before.
     */
    public Bitmap getAlbumImageForTrack(final MPDTrack track, int width, int height, boolean skipCache) throws ImageNotFoundException {
        if (null == track) {
            return null;
        }
        Bitmap image = null;

        // Try to get image from Albumname/Album artistname
        try {
            image = getAlbumImageFromAlbumNameArtistName(track.getTrackAlbum(), track.getTrackAlbumArtist(), width, height, skipCache);
        } catch (ImageNotFoundException e) {
        }
        if (null != image) {
            return image;
        }

        try {
            image = getAlbumImageFromAlbumNameArtistName(track.getTrackAlbum(), track.getTrackArtist(), width, height, skipCache);
        } catch (ImageNotFoundException e) {
        }
        if (null != image) {
            return image;
        }

        // Last resort, try just the name
        image = getAlbumImageFromName(track.getTrackAlbum(), width, height);

        if (null != image) {
            return image;
        } else {
            return null;
        }

    }

    /**
     * Starts an asynchronous fetch for the image of the given artist.
     *
     * @param artist Artist to fetch an image for.
     */
    public void fetchArtistImage(final MPDArtist artist) {
        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo == null) {
            return;
        }
        boolean isWifi = networkInfo.getType() == ConnectivityManager.TYPE_WIFI || networkInfo.getType() == ConnectivityManager.TYPE_ETHERNET;

        if (mWifiOnly && !isWifi) {
            return;
        }

        if (HTTPImageProvider.getInstance(mContext).getActive()) {

            HTTPImageProvider.getInstance(mContext).fetchArtistImage(artist, response -> new InsertArtistImageTask().execute(response), new ArtistFetchError() {

                @Override
                public void fetchJSONException(MPDArtist artist, JSONException exception) {

                }

                @Override
                public void fetchVolleyError(MPDArtist artist, VolleyError error) {
                    Log.v(TAG, "Local HTTP download failed");
                }
            });
        }
    }

    /**
     * Starts an asynchronous fetch for the image of the given album
     *
     * @param album Album to fetch an image for.
     */
    public void fetchAlbumImage(final MPDAlbum album) {
        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo == null) {
            return;
        }
        boolean isWifi = networkInfo.getType() == ConnectivityManager.TYPE_WIFI || networkInfo.getType() == ConnectivityManager.TYPE_ETHERNET;

        if (mWifiOnly && !isWifi) {
            return;
        }

        if (HTTPImageProvider.getInstance(mContext).getActive()) {

            HTTPImageProvider.getInstance(mContext).fetchAlbumImage(album, response -> new InsertAlbumImageTask().execute(response), new AlbumFetchError() {

                @Override
                public void fetchJSONException(MPDAlbum album, JSONException exception) {

                }

                @Override
                public void fetchVolleyError(MPDAlbum album, VolleyError error) {
                    Log.v(TAG, "Local HTTP download failed");
                }
            });
        }
    }

    /**
     * Starts an asynchronous fetch for the image of the given album
     *
     * @param track Track to be used for image fetching
     */
    public void fetchAlbumImage(final MPDTrack track) {
        MPDAlbum album = new MPDAlbum(track.getTrackAlbum());
        album.setArtistName(track.getTrackAlbumArtist());

        fetchAlbumImage(album);
    }

    /**
     * Registers a listener that gets notified when a new artist image was added to the dataset.
     *
     * @param listener Listener to register
     */
    public void registerOnNewArtistImageListener(onNewArtistImageListener listener) {
        if (null != listener) {
            synchronized (mArtistListeners) {
                mArtistListeners.add(listener);
            }
        }
    }

    /**
     * Unregisters a listener that got notified when a new artist image was added to the dataset.
     *
     * @param listener Listener to unregister
     */
    public void unregisterOnNewArtistImageListener(onNewArtistImageListener listener) {
        if (null != listener) {
            synchronized (mArtistListeners) {
                mArtistListeners.remove(listener);
            }
        }
    }

    /**
     * Registers a listener that gets notified when a new album image was added to the dataset.
     *
     * @param listener Listener to register
     */
    public void registerOnNewAlbumImageListener(onNewAlbumImageListener listener) {
        if (null != listener) {
            synchronized (mArtistListeners) {
                mAlbumListeners.add(listener);
            }
        }
    }

    /**
     * Unregisters a listener that got notified when a new album image was added to the dataset.
     *
     * @param listener Listener to unregister
     */
    public void unregisterOnNewAlbumImageListener(onNewAlbumImageListener listener) {
        if (null != listener) {
            synchronized (mArtistListeners) {
                mAlbumListeners.remove(listener);
            }
        }
    }


    /**
     * Interface implementation to handle errors during fetching of album images
     *
     * @param album Album that resulted in a fetch error
     */
    public void fetchJSONException(MPDAlbum album, JSONException exception) {
        Log.e(TAG, "Error fetching album: " + album.getName() + "-" + album.getArtistName());
        AlbumImageResponse imageResponse = new AlbumImageResponse();
        imageResponse.album = album;
        imageResponse.image = null;
        imageResponse.url = null;
        new InsertAlbumImageTask().execute(imageResponse);
    }

    /**
     * Called if a volley error occurs during internet communication.
     *
     * @param album {@link MPDAlbum} the error occured for.
     * @param error {@link VolleyError} that was emitted
     */
    public void fetchVolleyError(MPDAlbum album, VolleyError error) {
        Log.e(TAG, "VolleyError for album: " + album.getName() + "-" + album.getArtistName());

        if (error != null) {
            NetworkResponse networkResponse = error.networkResponse;
            /**
             * Rate limit probably reached. Discontinue downloading to prevent
             * ban on the servers.
             */
            if (networkResponse != null && networkResponse.statusCode == 503) {
                cancelAllRequests();
                return;
            }
        }

        AlbumImageResponse imageResponse = new AlbumImageResponse();
        imageResponse.album = album;
        imageResponse.image = null;
        imageResponse.url = null;
        new InsertAlbumImageTask().execute(imageResponse);
    }

    /**
     * Interface implementation to handle errors during fetching of artist images
     *
     * @param artist Artist that resulted in a fetch error
     */
    public void fetchJSONException(MPDArtist artist, JSONException exception) {
        Log.e(TAG, "Error fetching artist: " + artist.getArtistName());
        ArtistImageResponse imageResponse = new ArtistImageResponse();
        imageResponse.artist = artist;
        imageResponse.image = null;
        imageResponse.url = null;
        new InsertArtistImageTask().execute(imageResponse);
    }

    /**
     * Called if a volley error occurs during internet communication.
     *
     * @param artist {@link MPDArtist} the error occured for.
     * @param error  {@link VolleyError} that was emitted
     */
    public void fetchVolleyError(MPDArtist artist, VolleyError error) {
        Log.e(TAG, "VolleyError fetching: " + artist.getArtistName());

        if (error != null) {
            NetworkResponse networkResponse = error.networkResponse;
            /**
             * Rate limit probably reached. Discontinue downloading to prevent
             * ban on the servers.
             */
            if (networkResponse != null && networkResponse.statusCode == 503) {
                cancelAllRequests();
                return;
            }
        }
        ArtistImageResponse imageResponse = new ArtistImageResponse();
        imageResponse.artist = artist;
        imageResponse.image = null;
        imageResponse.url = null;
        new InsertArtistImageTask().execute(imageResponse);
    }

    /**
     * AsyncTask to insert the images to the SQLdatabase. This is necessary as the Volley response
     * is handled in the UI thread.
     */
    private class InsertArtistImageTask extends AsyncTask<ArtistImageResponse, Object, MPDArtist> {

        /**
         * Inserts the image to the database.
         *
         * @param params Pair of byte[] (containing the image itself) and MPDArtist for which the image is for
         * @return the artist model that was inserted to the database.
         */
        @Override
        protected MPDArtist doInBackground(ArtistImageResponse... params) {
            ArtistImageResponse response = params[0];

            if (response.image == null) {
                mDBManager.insertArtistImage(mContext, response.artist, response.image);
                return response.artist;
            }

            // Rescale them if to big
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(response.image, 0, response.image.length, options);
            if ((options.outHeight > MAXIMUM_IMAGE_RESOLUTION || options.outWidth > MAXIMUM_IMAGE_RESOLUTION)) {
                Log.v(TAG, "Image to big, rescaling");
                options.inJustDecodeBounds = false;
                Bitmap bm = BitmapFactory.decodeByteArray(response.image, 0, response.image.length, options);
                ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
                Bitmap.createScaledBitmap(bm, MAXIMUM_IMAGE_RESOLUTION, MAXIMUM_IMAGE_RESOLUTION, true).compress(Bitmap.CompressFormat.JPEG, IMAGE_COMPRESSION_SETTING, byteStream);
                if(byteStream.size() <= MAXIMUM_IMAGE_SIZE) {
                    mDBManager.insertArtistImage(mContext, response.artist, byteStream.toByteArray());
                }
            } else {
                if(response.image.length <= MAXIMUM_IMAGE_SIZE) {
                    mDBManager.insertArtistImage(mContext, response.artist, response.image);
                }
            }

            broadcastNewArtistImageInfo(response, mContext);

            return response.artist;
        }

        /**
         * Notifies the listeners about a change in the image dataset. Called in the UI thread.
         *
         * @param result Artist that was inserted in the database
         */
        protected void onPostExecute(MPDArtist result) {
            synchronized (mArtistListeners) {
                for (onNewArtistImageListener artistListener : mArtistListeners) {
                    artistListener.newArtistImage(result);
                }
            }
        }

    }

    /**
     * AsyncTask to insert the images to the SQLdatabase. This is necessary as the Volley response
     * is handled in the UI thread.
     */
    private class InsertAlbumImageTask extends AsyncTask<AlbumImageResponse, Object, MPDAlbum> {

        /**
         * Inserts the image to the database.
         *
         * @param params Pair of byte[] (containing the image itself) and MPDAlbum for which the image is for
         * @return the album model that was inserted to the database.
         */
        @Override
        protected MPDAlbum doInBackground(AlbumImageResponse... params) {
            AlbumImageResponse response = params[0];

            if (response.image == null) {
                mDBManager.insertAlbumImage(mContext, response.album, response.image);
                return response.album;
            }

            Log.v(TAG, "Inserting image for album: " + response.album.getName());
            // Rescale them if to big
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(response.image, 0, response.image.length, options);
            if ((options.outHeight > MAXIMUM_IMAGE_RESOLUTION || options.outWidth > MAXIMUM_IMAGE_RESOLUTION)) {
                Log.v(TAG, "Image to big, rescaling");
                options.inJustDecodeBounds = false;
                Bitmap bm = BitmapFactory.decodeByteArray(response.image, 0, response.image.length, options);
                ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
                Bitmap.createScaledBitmap(bm, MAXIMUM_IMAGE_RESOLUTION, MAXIMUM_IMAGE_RESOLUTION, true).compress(Bitmap.CompressFormat.JPEG, IMAGE_COMPRESSION_SETTING, byteStream);
                if(byteStream.size() <= MAXIMUM_IMAGE_SIZE) {
                    mDBManager.insertAlbumImage(mContext, response.album, byteStream.toByteArray());
                }
            } else {
                if(response.image.length <= MAXIMUM_IMAGE_SIZE) {
                    mDBManager.insertAlbumImage(mContext, response.album, response.image);
                }
            }

            broadcastNewAlbumImageInfo(response, mContext);

            return response.album;
        }

        /**
         * Notifies the listeners about a change in the image dataset. Called in the UI thread.
         *
         * @param result Album that was inserted in the database
         */
        protected void onPostExecute(MPDAlbum result) {
            synchronized (mAlbumListeners) {
                for (onNewAlbumImageListener albumListener : mAlbumListeners) {
                    albumListener.newAlbumImage(result);
                }
            }
        }
    }

    /**
     * Used to broadcast information about new available artwork to {@link BroadcastReceiver} like
     * the {@link at.weiss.matthias.musicala.application.background.WidgetProvider} to reload its artwork.
     *
     * @param artistImage Image response containing the artist that an image was inserted for.
     * @param context     Context used for broadcasting
     */
    private void broadcastNewArtistImageInfo(ArtistImageResponse artistImage, Context context) {
        Intent newImageIntent = new Intent(ACTION_NEW_ARTWORK_READY);

        newImageIntent.putExtra(INTENT_EXTRA_KEY_ARTIST_NAME, artistImage.artist.getArtistName());

        context.sendBroadcast(newImageIntent);
    }

    /**
     * Used to broadcast information about new available artwork to {@link BroadcastReceiver} like
     * the {@link at.weiss.matthias.musicala.application.background.WidgetProvider} to reload its artwork.
     *
     * @param albumImage Image response containing the album that an image was inserted for.
     * @param context    Context used for broadcasting
     */
    private void broadcastNewAlbumImageInfo(AlbumImageResponse albumImage, Context context) {
        Intent newImageIntent = new Intent(ACTION_NEW_ARTWORK_READY);

        newImageIntent.putExtra(INTENT_EXTRA_KEY_ALBUM_NAME, albumImage.album.getName());

        context.sendBroadcast(newImageIntent);
    }

    /**
     * Interface used for adapters to be notified about data set changes
     */
    public interface onNewArtistImageListener {
        void newArtistImage(MPDArtist artist);
    }

    /**
     * Interface used for adapters to be notified about data set changes
     */
    public interface onNewAlbumImageListener {
        void newAlbumImage(MPDAlbum album);
    }

    /**
     * Called if the connection state of the device is changing. This ensures no data is downloaded
     * if it is not intended (mobile data connection).
     */
    private class ConnectionStateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (null == netInfo) {
                return;
            }
            boolean isWifi = netInfo.getType() == ConnectivityManager.TYPE_WIFI || netInfo.getType() == ConnectivityManager.TYPE_ETHERNET;

            if (mWifiOnly && !isWifi) {
                // Cancel all downloads
                Log.v(TAG, "Cancel all downloads because of connection change");
                cancelAllRequests();
            }

        }
    }

    /**
     * This will cancel the last used album/artist image providers. To make this useful on connection change
     * it is important to cancel all requests when changing the provider in settings.
     */
    public void cancelAllRequests() {
        Log.v(TAG, "Cancel all download requests");
        MALPRequestQueue.getInstance(mContext).cancelAll(request -> true);
    }
}
