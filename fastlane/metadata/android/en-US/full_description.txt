musicala is a fork of the app M.A.L.P (https://f-droid.org/en/packages/org.gateshipone.malp/), a remote control app for mpd, the music player daemon (https://www.musicpd.org/).

This app is intended to be used on tablets only, if you're looking for an app to be used on smaller devices, consider using M.A.L.P.

Features:
 * Artist/Album/Tracks library browsing
 * Launcher widget
 * Notification with background service (optional, can be disabled in the settings)
 * Volume control if application is in background (only if notification is enabled & visible)
 * For tablets only
 * Basic playlist management (add songs to the list, replace list with new one)
 * Multiple server profiles
 * When cover art is available in your music library musicala can access it via HTTP

This software is free software, licensed under the GPLv3 or later license. The source code is available on Gitlab ( https://gitlab.com/matthias-weiss/musicala )
